package com.bcp.pe.tipocambiobcp.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bcp.pe.tipocambiobcp.beans.CurrencyBean
import com.bcp.pe.tipocambiobcp.data.DataSource

class CurrenciesListViewModel (val dataSource: DataSource) : ViewModel() {
    val currenciesLiveData = dataSource.getCurrencyList()

    fun getCurrencyForCode(currencyCode : String) : CurrencyBean? {
        return dataSource.getCurrencyForCode(currencyCode)
    }
}

class CurrenciesListViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CurrenciesListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CurrenciesListViewModel(
                dataSource = DataSource.getDataSource(context.resources)
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}