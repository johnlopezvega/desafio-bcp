package com.bcp.pe.tipocambiobcp.beans

import android.os.Parcel
import android.os.Parcelable

data class CurrencyBean(
    var country: String?,
    var name: String?,
    var countryName: String?,
    var countryCode: String?,
    var exchangeBuy: Double,
    var exchangeSell: Double,
    var codCurrency: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeString(name)
        parcel.writeString(countryName)
        parcel.writeString(countryCode)
        parcel.writeDouble(exchangeBuy)
        parcel.writeDouble(exchangeSell)
        parcel.writeString(codCurrency)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CurrencyBean> {
        override fun createFromParcel(parcel: Parcel): CurrencyBean {
            return CurrencyBean(parcel)
        }

        override fun newArray(size: Int): Array<CurrencyBean?> {
            return arrayOfNulls(size)
        }
    }
}