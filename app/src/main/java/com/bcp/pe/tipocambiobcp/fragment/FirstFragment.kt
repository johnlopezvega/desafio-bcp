package com.bcp.pe.tipocambiobcp.fragment

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bcp.pe.tipocambiobcp.R
import com.bcp.pe.tipocambiobcp.beans.CurrencyBean
import com.bcp.pe.tipocambiobcp.databinding.FragmentFirstBinding
import com.bcp.pe.tipocambiobcp.viewmodel.CurrenciesListViewModel
import com.bcp.pe.tipocambiobcp.viewmodel.CurrenciesListViewModelFactory

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private var sender: CurrencyBean? = null
    private var receiver: CurrencyBean? = null
    private val currenciesListViewModel by viewModels<CurrenciesListViewModel> {
        CurrenciesListViewModelFactory(this.requireContext())
    }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonSend.setOnClickListener {
            val bundle = bundleOf("typeOperation" to "sender", "previousCurrency" to receiver)
            if (!binding.amountSend.editText?.text.isNullOrEmpty()) {
                bundle.putString("textAmountSend", binding.amountSend.editText?.text.toString())
            }
            if (!binding.amountReceiver.editText?.text.isNullOrEmpty()) {
                bundle.putString("textAmountReceive", binding.amountReceiver.editText?.text.toString())
            }
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundle)
        }
        binding.buttonReceiver.setOnClickListener {
            val bundle = bundleOf("typeOperation" to "receiver", "previousCurrency" to sender)
            if (!binding.amountSend.editText?.text.isNullOrEmpty()) {
                bundle.putString("textAmountSend", binding.amountSend.editText?.text.toString())
            }
            if (!binding.amountReceiver.editText?.text.isNullOrEmpty()) {
                bundle.putString("textAmountReceive", binding.amountReceiver.editText?.text.toString())
            }
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundle)
        }
        binding.changeSelection.setOnClickListener {
            val temp = sender
            sender = receiver
            receiver = temp
            updateViewCurrencies(sender!!, receiver!!)
            val exchange = receiver!!.exchangeBuy / sender?.exchangeBuy!!
            if (!binding.amountSend.editText?.text.isNullOrEmpty()) {
                val senderValue = binding.amountSend.editText?.getText().toString().toDouble()
                binding.amountReceiver.editText!!.setText(String.format("%.3f",senderValue * exchange * 0.99))
            }
        }
        binding.amountSend.editText?.setOnEditorActionListener{ v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (!v.text.isNullOrEmpty()) {
                    val exchange = receiver!!.exchangeBuy / sender?.exchangeBuy!!
                    val senderValue = v.text.toString().toDouble()
                    binding.amountReceiver.editText!!.setText(String.format("%.3f", senderValue*exchange*0.99))
                }
                true
            } else {
                false
            }
        }
        binding.amountReceiver.editText?.setOnEditorActionListener{ v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (!v.text.isNullOrEmpty()) {
                    val exchange = sender?.exchangeBuy!! / receiver!!.exchangeBuy
                    val receiverValue = v.text.toString().toDouble()
                    binding.amountSend.editText!!.setText(String.format("%.3f", receiverValue*exchange*1.01))
                }
                true
            } else {
                false
            }
        }
        setFragmentResultListener("currencySelectedRequest") { key, bundle ->
            // We use a String here, but any type that can be put in a Bundle is supported
            val result = bundle.get("currencySelectedKey")
            val typeOperation:String? = bundle.getString("typeOperation")
            // Do something with the result...
            if (bundle.containsKey("textAmountSend")) {
                binding.amountSend.editText!!.setText(bundle.getString("textAmountSend"))
            }
            if (bundle.containsKey("textAmountReceive")) {
                binding.amountReceiver.editText!!.setText(bundle.getString("textAmountReceive"))
            }
            if ("sender" == typeOperation) {
                sender = result as CurrencyBean?
                receiver = bundle.getParcelable("previousCurrency")
                updateValueReceive()
            } else if ("receiver" == typeOperation) {
                receiver = result as CurrencyBean?
                sender = bundle.getParcelable("previousCurrency")
                updateValueReceive()
            }
            updateViewCurrencies(sender!!, receiver!!)
        }
        currenciesListViewModel.currenciesLiveData.observe(this.requireActivity()) {
            it?.let { it1 ->
                if (sender == null) {
                    sender = it1.firstOrNull { it2 ->
                        it2.codCurrency == "PEN"
                    }
                }
                if (receiver == null) {
                    receiver = it1.firstOrNull { it2 ->
                        it2.codCurrency == "USD"
                    }
                }
                updateViewCurrencies(sender!!, receiver!!)
            }
        }
    }

    private fun updateViewCurrencies(sender: CurrencyBean, receiver: CurrencyBean) {
        binding.buttonSend.text = sender.name
        binding.buttonReceiver.text = receiver.name
        val exchange = sender.exchangeBuy / receiver.exchangeBuy
        val buy = String.format("%.3f", exchange*0.99)
        val sell = String.format("%.3f", exchange*1.01)
        binding.textExchange.text = "Compra: ${buy} | Venta: ${sell}"
    }

    private fun updateValueReceive() {
        if (!binding.amountSend.editText?.text.isNullOrEmpty()) {
            val exchange = receiver!!.exchangeBuy / sender?.exchangeBuy!!
            val senderValue = binding.amountSend.editText?.text.toString().toDouble()
            binding.amountReceiver.editText!!.setText(String.format("%.3f", senderValue * exchange * 0.99))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}