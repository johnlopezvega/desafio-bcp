package com.bcp.pe.tipocambiobcp.data

import android.content.res.Resources
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bcp.pe.tipocambiobcp.R
import com.bcp.pe.tipocambiobcp.beans.CurrencyBean
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DataSource(resources: Resources) {
    private val initialCurrencyList = currencyList(resources)
    private val currencyLiveData = MutableLiveData(initialCurrencyList)

    fun currencyList(resources: Resources): List<CurrencyBean> {
        var jsonFileString: String =
            resources.openRawResource(R.raw.data_countries_exchange).bufferedReader()
                .use { it.readText() }
        Log.i("data", jsonFileString)
        val gson = Gson()
        val listCurrencyBeanType = object : TypeToken<List<CurrencyBean>>() {}.type
        var initialCurrencyList: List<CurrencyBean> =
            gson.fromJson(jsonFileString, listCurrencyBeanType)
        initialCurrencyList.forEachIndexed { idx, currency -> Log.i("data", "> Item $idx:\n$currency") }
        return initialCurrencyList
    }

    fun getCurrencyList(): LiveData<List<CurrencyBean>> {
        return currencyLiveData
    }

    fun getCurrencyForCode(code: String): CurrencyBean? {
        currencyLiveData.value?.let { flowers ->
            return flowers.firstOrNull{ it.codCurrency == code}
        }
        return null
    }

    companion object {
        private var INSTANCE: DataSource? = null

        fun getDataSource(resources: Resources): DataSource {
            return synchronized(DataSource::class) {
                val newInstance = INSTANCE ?: DataSource(resources)
                INSTANCE = newInstance
                newInstance
            }
        }
    }
}