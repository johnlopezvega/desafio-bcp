package com.bcp.pe.tipocambiobcp.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bcp.pe.tipocambiobcp.R
import com.bcp.pe.tipocambiobcp.adapters.CurrencyAdapter
import com.bcp.pe.tipocambiobcp.beans.CurrencyBean
import com.bcp.pe.tipocambiobcp.databinding.FragmentSecondBinding
import com.bcp.pe.tipocambiobcp.viewmodel.CurrenciesListViewModel
import com.bcp.pe.tipocambiobcp.viewmodel.CurrenciesListViewModelFactory

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private val currenciesListViewModel by viewModels<CurrenciesListViewModel> {
        CurrenciesListViewModelFactory(this.requireContext())
    }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currenciesListViewModel.currenciesLiveData.observe(this.requireActivity()) {
            it?.let {
                val currenciesAdapter = CurrencyAdapter(it.toTypedArray(), context) {
                    adapterOnClick(it)
                }
                binding.listCurrencyRecycler.adapter = currenciesAdapter
            }
        }
    }

    private fun adapterOnClick(currencySelected: CurrencyBean) {
        setFragmentResult(
            "currencySelectedRequest",
            bundleOf(
                "currencySelectedKey" to currencySelected,
                "typeOperation" to arguments?.getString("typeOperation"),
                "previousCurrency" to arguments?.get("previousCurrency"),
                "textAmountSend" to arguments?.getString("textAmountSend"),
                "textAmountReceive" to arguments?.getString("textAmountReceive")
            )
        )
        findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}