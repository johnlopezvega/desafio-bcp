package com.bcp.pe.tipocambiobcp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bcp.pe.tipocambiobcp.R
import com.bcp.pe.tipocambiobcp.beans.CurrencyBean
import java.util.*

class CurrencyAdapter(private val dataSet: Array<CurrencyBean>, private val context: Context?, private val onClick: (CurrencyBean) -> Unit) :
    RecyclerView.Adapter<CurrencyAdapter.ViewHolder>(){
    class ViewHolder(view: View, val onClick: (CurrencyBean) -> Unit) : RecyclerView.ViewHolder(view) {
        val textTitleCurrency: TextView
        val imgCurrency: ImageView
        val textValueCurrency: TextView
        var currentCurrency: CurrencyBean? = null

        init {
            // Define click listener for the ViewHolder's View.
            textTitleCurrency = view.findViewById(R.id.text_title_currency)
            imgCurrency = view.findViewById(R.id.img_currency)
            textValueCurrency = view.findViewById(R.id.text_value_currency)
            view.setOnClickListener{
                currentCurrency?.let { onClick(it) }
            }
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.currency_row_item, viewGroup, false)
        return ViewHolder(view, onClick)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.textTitleCurrency.text = dataSet[position].name
        viewHolder.textValueCurrency.text =
            "1 EUR = ${dataSet[position].exchangeBuy} ${dataSet[position].codCurrency}"
        val idImage: Int = context!!.resources.getIdentifier(
            dataSet[position].countryCode!!.lowercase(
            Locale.getDefault()
        ), "drawable", context.packageName)
        viewHolder.imgCurrency.setImageResource(idImage)
        viewHolder.currentCurrency = dataSet[position]
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}